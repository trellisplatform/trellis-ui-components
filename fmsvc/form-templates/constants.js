export const FORM_DEF_INPUT_TYPE = {
  TEXT: 'text',
  NUMBER: 'number',
  CURRENCY: 'currency',
  PHONE_NUMBER: 'phone_number',
  TEXT_LONG: 'text_long',
  RADIO: 'radio',
  SLIDER: 'slider',
  FILE: 'file',
  FILE_V2: 'file_v2',
  SELECT: 'select',
  SELECT_MULTI: 'select_multi',
  BULLET: 'bullet',
  DATE: 'date',
  TIMELINE: 'timeline',
  PUBLIC_IMAGE: 'public_image',
  STATIC_MODAL: 'static_modal',
  ADDRESS: 'address',
  URL: 'url',
  DATA_FILE: 'data_file',
  CALCULATED: 'calculated',
  CHECK_LIST: 'check_list',
  BUTTON_TOGGLE: 'button_toggle',
  DRAGGABLE_LIST: 'draggable_list',
  RICH_TEXTEDITOR: 'rich_texteditor',
  STATIC_TEXT: 'static_text',
}

export const FORM_DEF_INPUT_TYPES = [
  { text: 'Text', value: FORM_DEF_INPUT_TYPE.TEXT },
  { text: 'Number', value: FORM_DEF_INPUT_TYPE.NUMBER },
  { text: 'Currency', value: FORM_DEF_INPUT_TYPE.CURRENCY },
  { text: 'Phone Number', value: FORM_DEF_INPUT_TYPE.PHONE_NUMBER },
  { text: 'Text Long', value: FORM_DEF_INPUT_TYPE.TEXT_LONG },
  { text: 'Radio', value: FORM_DEF_INPUT_TYPE.RADIO },
  { text: 'Slider', value: FORM_DEF_INPUT_TYPE.SLIDER },
  { text: 'File', value: FORM_DEF_INPUT_TYPE.FILE },
  { text: 'File V2 Design', value: FORM_DEF_INPUT_TYPE.FILE_V2 },
  { text: 'Select', value: FORM_DEF_INPUT_TYPE.SELECT },
  { text: 'Select Multiple', value: FORM_DEF_INPUT_TYPE.SELECT_MULTI },
  { text: 'Bullet', value: FORM_DEF_INPUT_TYPE.BULLET },
  { text: 'Date', value: FORM_DEF_INPUT_TYPE.DATE },
  { text: 'Timeline', value: FORM_DEF_INPUT_TYPE.TIMELINE },
  { text: 'Public Image', value: FORM_DEF_INPUT_TYPE.PUBLIC_IMAGE },
  { text: 'Modal', value: FORM_DEF_INPUT_TYPE.STATIC_MODAL },
  { text: 'Address', value: FORM_DEF_INPUT_TYPE.ADDRESS },
  { text: 'URL', value: FORM_DEF_INPUT_TYPE.URL },
  { text: 'Data File', value: FORM_DEF_INPUT_TYPE.DATA_FILE },
  { text: 'Calculated', value: FORM_DEF_INPUT_TYPE.CALCULATED },
  { text: 'Check list', value: FORM_DEF_INPUT_TYPE.CHECK_LIST },
  { text: 'Draggable list', value: FORM_DEF_INPUT_TYPE.DRAGGABLE_LIST },
  { text: 'Rich Text Editor', value: FORM_DEF_INPUT_TYPE.RICH_TEXTEDITOR },
  { text: 'Static Text', value: FORM_DEF_INPUT_TYPE.STATIC_TEXT },
]

export const FORM_DEF_DATA_SOURCE_TYPE = {
  INLINE: 'inline',
  INLINE_INDEX: 'inline_index',
  ENTITY: 'entity',
  ENTITY_COMMON: 'entity_common',
  CYNDX: 'cyndx',
}

export const FORM_DEF_DATA_SOURCE_TYPES = [
  { text: 'Inline', value: FORM_DEF_DATA_SOURCE_TYPE.INLINE },
  { text: 'Inline Index', value: FORM_DEF_DATA_SOURCE_TYPE.INLINE_INDEX },
  { text: 'Entity', value: FORM_DEF_DATA_SOURCE_TYPE.ENTITY },
  { text: 'Common Entity', value: FORM_DEF_DATA_SOURCE_TYPE.ENTITY_COMMON },
  { text: 'Cyndx', value: FORM_DEF_DATA_SOURCE_TYPE.CYNDX },
]

export const FORM_DATA_FILE_TYPE = {
  OPPORTUNITY_DOCUMENT: 'OPPORTUNITY_DOCUMENT',
  ISSUER_DOCUMENT: 'ISSUER_DOCUMENT',
}

export const FORM_DATA_FILE_TYPES = [
  { text: 'Opportunity Document', value: FORM_DATA_FILE_TYPE.OPPORTUNITY_DOCUMENT },
  { text: 'Issuer Document', value: FORM_DATA_FILE_TYPE.ISSUER_DOCUMENT },
]

export const FORM_BRANCH_EXPR = {
  EQ: '-eq',
  CONTAINS: '-contains',
  IS_CONTAINED: '-is_contained',
}

export const FORM_BRANCH_EXPRS = [
  { text: 'Equal', value: FORM_BRANCH_EXPR.EQ },
  { text: 'Contains', value: FORM_BRANCH_EXPR.CONTAINS },
  { text: 'Is Contained', value: FORM_BRANCH_EXPR.IS_CONTAINED },
]

export const FORM_COMMON = 'common'

export const FORM_SELECT_QUERY_COMPARATOR = {
  EQUALS: 'eq',
  STARTS_WITH: 'starts_with',
}

export const FORM_SELECT_QUERY_COMPARATORS = [
  { text: 'Equals', value: FORM_SELECT_QUERY_COMPARATOR.EQUALS },
  { text: 'Starts With', value: FORM_SELECT_QUERY_COMPARATOR.STARTS_WITH },
]

export const PRIMARY_ENTITY_ONBOARD_PREFORM_ID = 'Onboarding.Entity.Preform.New'
export const BENEFICIARY_ENTITY_ONBOARD_PREFORM_ID = 'Onboarding.Entity.Preform.Beneficial'

export const FORM_CALCULATED_ACTIONS = [
  { text: 'Multiply', value: 'multiply' },
  { text: 'Divide', value: 'divide' },
]

export const FORM_CALCULATED_FIELD_TYPE = [
  { text: 'Constant', value: 'const' },
  { text: 'Variable', value: 'var' },
]

export const FORM_CALCULATED_ROUND_TYPE = [
  { text: 'Round Up', value: 'up' },
  { text: 'Round Down', value: 'down' },
]

export const DOC_ACTION_REQUIRED = {
  SIGNATURE_REQUIRED: 'SIGNATURE_REQUIRED',
  REVIEW_REQUIRED: 'REVIEW_REQUIRED',
}

export const DL_NDA_REDIRECT_NOTIFICATION = 'You have successfully signed the NDA. The Document has been sent to Issuer for counter signing.'

export const MULTIPLE_SEC_TABLE_COL_AVAILABLE_HEADER_TYPES = [
  FORM_DEF_INPUT_TYPE.TEXT,
  FORM_DEF_INPUT_TYPE.URL,
  FORM_DEF_INPUT_TYPE.RADIO,
  FORM_DEF_INPUT_TYPE.SELECT,
]
