import { api } from '@/api'
import Papa from 'papaparse'
import { FORM_DEF_INPUT_TYPE, FORM_DEF_DATA_SOURCE_TYPE, FORM_BRANCH_EXPR } from '@/constants'
import moment from 'moment'

const checkContains = (lvalue, rvalue) => {
  let valid = false

  if (lvalue &&
    Array.isArray(lvalue) &&
    !Array.isArray(rvalue)
  ) {
    if (typeof rvalue !== 'object' && lvalue.indexOf(rvalue) !== -1) {
      valid = true
    }
    if (typeof rvalue === 'object' &&
      lvalue.filter(v => v && v.id === rvalue.id).length.length > 0
    ) {
      valid = true
    }
  }

  if (lvalue &&
    Array.isArray(lvalue) &&
    Array.isArray(rvalue)
  ) {
    const results = rvalue.map(rV => {
      if (typeof rV === 'object') {
        // if it's included in lValue
        return lvalue.filter(lV => {
          if (rV && lV && rV.id === lV.id) {
            return true
          }
        }).length > 0
      }

      if (typeof rV !== 'object') {
        // if it's included in lValue
        return lvalue.filter(lV => {
          if (lV === rV) {
            return true
          }
        }).length > 0
      }
    })

    valid = results.indexOf(false) === -1
  }

  return valid
}

export function branchCompare (lvalue, rvalue, expr) {
  let valid = false

  switch (expr) {
    case FORM_BRANCH_EXPR.EQ:
      // eslint-disable-next-line
      if (lvalue == rvalue) {
        valid = true
      }
      break
    case FORM_BRANCH_EXPR.CONTAINS:
      valid = checkContains(lvalue, rvalue)
      break
    case FORM_BRANCH_EXPR.IS_CONTAINED:
      valid = checkContains(rvalue, lvalue)
      break
  }

  return valid
}

/**
  * document.file - file input object
  * document.s3_key - path to file
  *
  * options.isCommon
  */
export const uploadDocument = async (document, options, progressCallback) => {
  const formData = new FormData()
  formData.append('file', document.file)
  // formData.append('s3_key', document.s3_key)
  formData.append('request_payload', JSON.stringify(document.request_payload))

  const apiResponse = await api.upload('document', formData, options, progressCallback)
  return apiResponse.data
}

export const updateDocument = async (document) => {
  const info = {
    details: document.details,
  }

  const apiResponse = await api.patch(
    `document/${document.document_id}`,
    info,
  )
  return apiResponse.data
}
/**
  * id - doc id
  *
  */
export const deleteDocument = async (id, apiOpts) => {
  const res = await api.delete(`document/${id}`, apiOpts)

  return res
}

/**
  * document.file - file input object
  * document.s3_key - path to file
  *
  * options.isCommon
  */
export const getDocumentById = async (documentId, options) => {
  const apiResponse = await api.get(`document/${documentId}`, null, options)
  return apiResponse.data
}

const NON_ALLOWED_EXPORT_INPUT_TYPES = [
  FORM_DEF_INPUT_TYPE.FILE,
]
const SEPARATOR = '||'

const getExportableFieldsOfSection = (section) => {
  const defs = []
  for (let i = 0; i < section.definition.length; i++) {
    const def = section.definition[i]

    // timeline includes 2 header - label and text
    if (def.input === FORM_DEF_INPUT_TYPE.TIMELINE) {
      defs.push({
        ...def,
        label: `${def.label} ${def.labelName || 'Label'}`,
        timeLineField: 'label',
        originalLabel: def.label,
      })
      defs.push({
        ...def,
        label: `${def.label} ${def.textName || 'Text'}`,
        timeLineField: 'text',
        originalLabel: def.label,
      })
    } else {
      defs.push(def)
    }
  }

  return defs
}

const getDefEntities = (def) => {
  if (def.input === FORM_DEF_INPUT_TYPE.RADIO ||
    def.input === FORM_DEF_INPUT_TYPE.SELECT ||
    def.input === FORM_DEF_INPUT_TYPE.SELECT_MULTI) {
    if (def.dataSource === FORM_DEF_DATA_SOURCE_TYPE.INLINE) {
      return Promise.resolve(def.data.map(ele => ({
        id: ele,
        name: ele,
      })))
    } else {
      return api.get(`data-entities/${def.entity}`, null, { base: def.dataSrc === FORM_DEF_DATA_SOURCE_TYPE.ENTITY_COMMON ? 'common' : '' })
      .then(axiosResponse => {
        return axiosResponse.data.value
      })
    }
  } else {
    return Promise.resolve([])
  }
}

export const downloadSectionTemplateCsv = (section) => {
  const rows = []
  const downloadEntities = true

  const validationRow = []
  const defs = getExportableFieldsOfSection(section)
  defs.map(def => {
    let str = ''
    if (def.input === FORM_DEF_INPUT_TYPE.SLIDER) {
      str = `${Number.isNaN(def.max) ? '' : `Max: ${def.max}`}, ${Number.isNaN(def.min) ? '' : `Min: ${def.min}`}`
    } else if (def.input === FORM_DEF_INPUT_TYPE.DATE) {
      str = 'YYYY-MM-DD'
    } else if (def.input === FORM_DEF_INPUT_TYPE.RADIO ||
      def.input === FORM_DEF_INPUT_TYPE.SELECT) {
      str = `value from ${def.label}.csv`
    } else if (def.input === FORM_DEF_INPUT_TYPE.SELECT_MULTI) {
      str = `${SEPARATOR} separated values from ${def.label}.csv`
    } else if (def.input === FORM_DEF_INPUT_TYPE.TIMELINE) {
      str = `${SEPARATOR} separated values`
    } else if (def.input === FORM_DEF_INPUT_TYPE.BULLET) {
      str = `${SEPARATOR} separated values`
    } else {
      str = `${def.regex ? def.regex : ''}`
    }
    validationRow.push(str)
  })

  const requireRow = []
  defs.map(def => {
    if (NON_ALLOWED_EXPORT_INPUT_TYPES.indexOf(def.input) !== -1) {
      requireRow.push('skipped')
    } else {
      requireRow.push(`${def.mandatory ? 'required' : 'optional'}`)
    }
  })

  const headerRow = []
  defs.map(def => headerRow.push(`${def.label}${def.mandatory ? '*' : ''}`))

  if (downloadEntities) {
    Promise.all(defs.map(getDefEntities))
      .then(resps => {
        defs.forEach((def, index) => {
          if (def.input === FORM_DEF_INPUT_TYPE.RADIO ||
            def.input === FORM_DEF_INPUT_TYPE.SELECT ||
            def.input === FORM_DEF_INPUT_TYPE.SELECT_MULTI) {
            const rows = resps[index].map(ele => [ele.name])
            const csvContent = 'data:text/csv;charset=utf-8,' + Papa.unparse(rows)
            downloadAsFile(csvContent, `${def.label}.csv`)
          }
        })
      })
  }

  rows.push(validationRow)
  rows.push(requireRow)
  rows.push(headerRow)

  // const csvContent = 'data:text/csv;charset=utf-8,' + rows.map(e => e.map(i => `"${i}"`).join(',')).join('\n')
  const csvContent = 'data:text/csv;charset=utf-8,' + Papa.unparse(rows)

  downloadAsFile(csvContent, `${section.name}.csv`)
}

export const parseSectionTemplateCsv = async (file, section) => {
  const fileContent = await readFileContent(file)
  const allRowData = Papa.parse(fileContent, {
    skipEmptyLines: true,
  })
  const csvRows = allRowData.data.slice(3) // extract headers
  const defs = getExportableFieldsOfSection(section)
  const resps = await Promise.all(defs.map(getDefEntities))
  defs.forEach((def, i) => {
    def.allData = resps[i]
  })

  const finalRows = []

  let errors = []
  const addError = (rowNumber, message) => {
    errors.push(`Row ${rowNumber + 4}: ${message}`)
  }
  // get final rows
  csvRows.forEach((csvRow, rowNum) => {
    const row = {}
    defs.forEach((def, ind) => {
      // disallow non allowed types
      if (NON_ALLOWED_EXPORT_INPUT_TYPES.indexOf(def.input) !== -1) {
        return
      }

      // format values properly
      let value = csvRow[ind]
      if (def.input === FORM_DEF_INPUT_TYPE.DATE) {
        if (value && !moment(value, 'YYYY-MM-DD').isValid()) {
          addError(rowNum, `${value} is not valid ${def.label}`)
        }
        value = moment(value, 'YYYY-MM-DD').format('MM/DD/YYYY')
      } else if (def.input === FORM_DEF_INPUT_TYPE.SLIDER) {
        value = +value
      } else if (def.input === FORM_DEF_INPUT_TYPE.BULLET) {
        value = value
          ? value.split(SEPARATOR)
          : []
      } else if (def.input === FORM_DEF_INPUT_TYPE.TIMELINE) {
        if (def.timeLineField === 'label') {
          value = value
            ? value.split(SEPARATOR).map(ele => ({ label: ele }))
            : []
        } else if (def.timeLineField === 'text') {
          const texts = value ? value.split(SEPARATOR) : []
          if (texts.length !== row[def.id].length) {
            addError(rowNum, `${def.originalLabel} does not match with ${def.labelName || 'Label'} and ${def.textName || 'Text'}`)
          }
          value = row[def.id].map((ele, i) => {
            ele.text = texts[i] || ''
            return ele
          })
        }
      } else if (def.input === FORM_DEF_INPUT_TYPE.RADIO ||
          def.input === FORM_DEF_INPUT_TYPE.SELECT) {
        if (value) {
          const item = def.allData.find(ele => ele.name === value)
          if (def.dataSource === FORM_DEF_DATA_SOURCE_TYPE.INLINE) {
            value = item ? item.name : ''
          } else {
            value = item || ''
          }
          if (!item) {
            addError(rowNum, `${value} could not found from ${def.label}.csv`)
          }
        }
      } else if (def.input === FORM_DEF_INPUT_TYPE.SELECT_MULTI) {
        const values = value ? value.split(SEPARATOR) : []
        if (values.length > 0) {
          value = values.map(val => {
            const item = def.allData.find(ele => ele.name === val)
            if (!item) {
              addError(rowNum, `${val} could not found from ${def.label}.csv`)
            }
            if (def.dataSource === FORM_DEF_DATA_SOURCE_TYPE.INLINE) {
              return item ? item.name : ''
            } else {
              return item || ''
            }
          })
          .filter(val => val)
        }
      }
      row[def.id] = value
    })
    finalRows.push(row)
  })

  // validate final rows
  errors = errors.concat(await validateSectionVals(finalRows, defs))
  if (errors.length > 0) {
    throw errors
  }
  return finalRows
}

export const validateSectionVals = (rows, defs) => {
  const errors = []
  const addError = (rowNumber, message) => {
    errors.push(`Row ${rowNumber + 4}: ${message}`)
  }

  rows.forEach((row, rowInd) => {
    defs.forEach((def, ind) => {
      // disallow non allowed types
      if (NON_ALLOWED_EXPORT_INPUT_TYPES.indexOf(def.input) !== -1) {
        return
      }

      const val = row[def.id]
      if (def.input === FORM_DEF_INPUT_TYPE.SLIDER) {
        if (def.mandatory && (!val && val !== 0)) {
          addError(rowInd, `${def.label} is required`)
        } else if (val && Number.isNaN(val)) {
          addError(rowInd, `${val} is not valid ${def.label}`)
        } else if (def.max && !Number.isNaN(def.max) && val > def.max) {
          addError(rowInd, `${val} should not be higher than ${def.max}`)
        } else if (def.min && !Number.isNaN(def.min) && val < def.min) {
          addError(rowInd, `${val} should not be lower than ${def.min}`)
        }
      } else if (def.input === FORM_DEF_INPUT_TYPE.DATE) {
        if (def.mandatory && !val) {
          addError(rowInd, `${def.label} is required`)
        } else if (val && !moment(val, 'MM/DD/YYYY').isValid()) {
          addError(rowInd, `${val} is not valid ${def.label}`)
        }
      } else if (def.input === FORM_DEF_INPUT_TYPE.BULLET) {
        if (def.mandatory && val.length === 0) {
          addError(rowInd, `${def.label} is required`)
        }
      } else if (def.input === FORM_DEF_INPUT_TYPE.TIMELINE) {
        if (def.mandatory && val.length === 0) {
          addError(rowInd, `${def.label} is required`)
        }
      } else if (def.input === FORM_DEF_INPUT_TYPE.RADIO ||
        def.input === FORM_DEF_INPUT_TYPE.SELECT) {
        if (def.mandatory && !val) {
          addError(rowInd, `${def.label} is required`)
        }
      } else if (def.input === FORM_DEF_INPUT_TYPE.SELECT_MULTI) {
        if (def.mandatory && val.length === 0) {
          addError(rowInd, `${def.label} is required`)
        }
      } else {
        if (def.mandatory && !val) {
          addError(rowInd, `${def.label} is required`)
        } else if (val && def.regex && !RegExp(def.regex).test(val)) {
          addError(rowInd, `${val} is not valid ${def.regex}`)
        }
      }
    })
  })

  return errors
}

export function downloadAsFile (content, filename) {
  const anchor = document.createElement('a')
  anchor.href = content
  anchor.target = '_blank'
  anchor.download = filename
  anchor.click()
}

export function readFileContent (file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onload = function () {
      resolve(reader.result)
    }
    reader.readAsText(file)
  })
}
