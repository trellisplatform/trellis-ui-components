import { api } from '@/api'

export const getInvestmentNotes = async (id, options, paymentId, apiOpts, isIssuer) => {
  const apiResponse = await api.get(getPaymentUrl(isIssuer, id, paymentId), options, apiOpts)
  return apiResponse.data
}

export const sendInvestmentNote = async (id, body, paymentId, apiOpts, isIssuer) => {
  const apiResponse = await api.post(getPaymentUrl(isIssuer, id, paymentId), body, apiOpts)
  return apiResponse.data
}

export const sendPaymentAcknowledgeMent = async (id, body, paymentId, apiOpts, isIssuer) => {
  const apiResponse = await api.post(getPaymentUrl(isIssuer, id, paymentId), body, apiOpts)
  return apiResponse.data
}

// const getUrl = (isIssuer, id) => {
//   if (isIssuer) {
//     return `opportunities/investments/${id}/note`
//   } else {
//     return `investments/${id}/note`
//   }
// }

const getPaymentUrl = (isIssuer, id, paymentId) => {
  if (isIssuer) {
    return `opportunities/investments/${id}/payment/${paymentId}`
  } else {
    return `investments/${id}/payment/${paymentId}`
  }
}
