import { shallowMount } from '@vue/test-utils'
import DenseItem from './DenseItem.vue'
test('DenseItem component test', () => {
  const $getPortal = () => {}
  const $date = () => {}
  const wrapper = shallowMount(DenseItem, {
    propsData: {
      item: {source: '', response: ''},
    },
    mocks: {
      $getPortal,
      $date,
      CONSTS: {PORTAL: {}},
    },
  })
  wrapper.find('.card-item').trigger('click')
  expect(wrapper.emitted('hideContent')).toHaveLength(1)
})