const routes = []

// if (process.env.NODE_ENV === 'development') {
routes.push({
  name: 'StyleGuide',
  path: '/style-guide',
  component: () => import('@/components/common/views/style-guide/Index'),
})
// }

export default routes
