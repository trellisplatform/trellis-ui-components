
import { shallowMount } from '@vue/test-utils'
import OverviewNavHeader from './OverviewNavHeader.vue'
test('OverviewNavHeader component test', () => {
  const wrapper = shallowMount(OverviewNavHeader)
    wrapper.find('.overview-nav-header').trigger('click')
    expect(wrapper.emitted('click')).toHaveLength(1)
  })