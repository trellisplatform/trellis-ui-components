const ISSUER_ONBOARDING_P1_FORM_ID = 'DRS.IOPI'
// const MARKET_INFORMATION_FORM_ID = 'DRS.MI'
const ISSUER_ONBOARDING_P2_FORM_ID = 'DRS.IOPII'
// const INTERNAL_DRS_FORM_ID = 'IDRS'
const FINANCIALS_FORM_ID = 'DRS.F.v2'
const FUND_SPONSOR_FORM_ID = 'FDRSS.V2'
const FUND_OPPORTUNITY_FORM_ID = 'FDRSOF.V2'
const DRS_FINANCIAL = 'GD.FI.Final'
const ISSUER_TAB_DRS_MASTER = 'IssuerTab.DRS.Master.v2'
const OVERVIEW_TAB_DRS_MASTER = 'Overview_DRS_Master_v2'
const OPPORTUNITY_DRS_MASTER = 'OpportunityTab.DRS.Master'
const FUND_OVERVIEW_FORM_ID = 'Fund.DRS.Overview.Form'
const SPV_OVERVIEW = 'SPV.DRS.Overview.Form'
const SPV_COMPANY = 'SPV.DRS.Company.Form'
const SPV_SPONSOR = 'SPV.DRS.Sponsor.Form'

const findResponseByFormId = (formData, internalName) => {
  const form = formData.find(data => data.internal_name === internalName)

  try {
    return form.response.response
  } catch (err) {
    return null
  }
}

export const extractDot = (obj) => {
  const newObj = {}
  for (const key in obj) {
    newObj[key.split('.')[key.split('.').length - 1]] = obj[key]
  }

  return newObj
}

const getPlainObjectTopLevel = (obj) => {
  const newObj = {}
  for (const key in obj) {
    if (key.startsWith('$$')) {
      continue
    }
    if (Array.isArray(obj[key])) {
      newObj[key] = obj[key]
    } else if (obj[key]) {
      for (const child in obj[key]) {
        newObj[child] = obj[key][child]
      }
    }
  }

  return newObj
}

const getFullFomData = (formData, formId) => {
  const response = findResponseByFormId(formData, formId)

  try {
    const data = getPlainObjectTopLevel(response)

    return data
  } catch (err) {
    return null
  }
}

const getSectionData = (formData, formId, sectionId) => {
  const response = findResponseByFormId(formData, formId)

  try {
    const data = response[sectionId]

    if (Array.isArray(data)) {
      return data.map(item => extractDot(item))
    }
    return extractDot(data)
  } catch (err) {
    return null
  }
}

const getMarketInformation = (oppFormData) => {
  const response = findResponseByFormId(oppFormData, OPPORTUNITY_DRS_MASTER)

  try {
    const data = getPlainObjectTopLevel(response)

    return data
  } catch (err) {
    return null
  }
}

const getFormMetaData = (formData, formId) => {
  const form = formData.find(data => data.internal_name === formId)
  try {
    return {
      ...form.response,
      response: {},
    }
  } catch (err) {
    return null
  }
}

export default (id, oppFormData, issuerFormData) => {
  let data = null
  oppFormData = oppFormData || []
  issuerFormData = issuerFormData || []
  switch (id) {
    // Opp forms
    case 'product-services':
      data = getSectionData(oppFormData, OPPORTUNITY_DRS_MASTER, 'OpportunityTab.DRS.Master.PSData')
      break

    case 'product-services-c':
      data = getSectionData(oppFormData, OVERVIEW_TAB_DRS_MASTER, 'Overview.DRS.CompanyDescription')
      break

    case 'product-services-form':
      data = getFullFomData(oppFormData, OPPORTUNITY_DRS_MASTER)
      break

    case 'product-service-form-meta':
      data = getFormMetaData(oppFormData, OVERVIEW_TAB_DRS_MASTER)
      break

    // Issuer forms
    case 'issuer-tab-data':
      data = getFullFomData(issuerFormData, ISSUER_TAB_DRS_MASTER)
      break

    case 'company-story':
      data = getFullFomData(issuerFormData, ISSUER_ONBOARDING_P1_FORM_ID)
      break

    case 'overview-tab-drs-master':
      data = getFullFomData(oppFormData, OVERVIEW_TAB_DRS_MASTER)
      break

    case 'funding-history':
      data = getSectionData(issuerFormData, ISSUER_ONBOARDING_P2_FORM_ID, 'DRS.IOPII.DH')
      break

    case 'company-description':
      data = getFullFomData(oppFormData, OVERVIEW_TAB_DRS_MASTER)
      break

    case 'lead-investors':
       data = getSectionData(issuerFormData, ISSUER_ONBOARDING_P2_FORM_ID, 'DRS.IOPII.LI')
       break

    case 'market-info':
      data = getMarketInformation(oppFormData)
      break

    case 'internal-drs':
      data = getFullFomData(oppFormData, OVERVIEW_TAB_DRS_MASTER)
      break

    case 'use-of-funds':
      data = getSectionData(oppFormData, OPPORTUNITY_DRS_MASTER, 'OpportunityTab.DRS.Master.UseofFunds')
      break

    case 'financials':
      data = getFullFomData(oppFormData, FINANCIALS_FORM_ID)
      break

    case 'captable':
      data = getSectionData(issuerFormData, ISSUER_TAB_DRS_MASTER, 'IssuerTab.DRS.Master.CapTable')
      break

    case 'drs-finanical':
      data = getFullFomData(issuerFormData, DRS_FINANCIAL)
      break

    // Sponsor forms
    case 'fund-sponsor':
      data = getFullFomData(issuerFormData, FUND_SPONSOR_FORM_ID)
      break

    case 'spv-overview':
      data = getFullFomData(oppFormData, SPV_OVERVIEW)
      break

    case 'spv-company':
      data = getFullFomData(oppFormData, SPV_COMPANY)
      break

    case 'spv-sponsor':
      data = getFullFomData(issuerFormData, SPV_SPONSOR)
      break

    case 'fund-opportunity':
      data = getFullFomData(oppFormData, FUND_OPPORTUNITY_FORM_ID)
      break

    case 'fund-overview':
      data = getFullFomData(oppFormData, FUND_OVERVIEW_FORM_ID)
      break
  }

  return data
}
