export const financialSampleData = {
    results: [
        {
            opportunity_id: 'f46f9c02-d594-48a5-b86b-68345b8aa365',
            rev_data: [
                {
                    year: 2021,
                    value: '287133',
                },
                {
                    year: 2022,
                    value: '2719290',
                },
                {
                    year: 2023,
                    value: '9829531',
                },
            ],
            exp_data: [
                {
                    year: 2021,
                    value: '1913265',
                },
                {
                    year: 2022,
                    value: '4086052',
                },
                {
                    year: 2023,
                    value: '6537147',
                },
            ],
            ebitda_data: [
                {
                    year: 2021,
                    value: '-1844779',
                },
                {
                    year: 2022,
                    value: '-2396814',
                },
                {
                    year: 2023,
                    value: '1671485',
                },
            ],
            cfin_data: [
                {
                    year: 2022,
                    value: '438062',
                },
                {
                    year: 2023,
                    value: '2182908',
                },
            ],
            cfout_data: [
                {
                    year: 2022,
                    value: '2135126',
                },
                {
                    year: 2023,
                    value: '508023',
                },
            ],
            funding_history: {
                history: [
                    {
                        name: 'Series Seed - 1 Preferred',
                        amount: '953567',
                        open_date: '2017-05-01',
                        pre_money: '',
                        close_date: '2020-07-29',
                    },
                    {
                        name: 'Series Seed - 2 Preferred',
                        amount: '2077497',
                        open_date: '2018-05-01',
                        pre_money: '',
                        close_date: '2020-07-29',
                    },
                    {
                        name: 'Series A Preferred',
                        amount: '4193497',
                        open_date: '2020-01-01',
                        pre_money: '',
                        close_date: '2021-10-29',
                    },
                ],
            },
            financial_documents: {
                full: 's3_bucket',
                cash_flow: 's3_bucket',
                balance_sheet: 's3_bucket',
                income_statement: 's3_bucket',
            },
        },
    ],
}
