import { api } from '@/api'
import { PORTAL, DR_DOC_LOAD_FROM } from '@/constants'
import * as formSrvc from '@/components/common/services/forms'

import parseForm from './parse-form'
import { financialSampleData } from './financials'
import { env } from '@/components/common/constants'
import * as CONSTS from '@/constants'
export default class DigitalRoadshowService {
  constructor (portal) {
    this.portal = portal
    this.oppFormData = null
    this.issuerFormData = null
    this.CONSTS = CONSTS
  }

  async getDocumentById (documentId, options) {
    const apiResponse = await api.get(`document/${documentId}`, null, options)
    return apiResponse.data
  }

  async getTenantDocumentById (documentId) {
    const apiResponse = await api.get(`mydocuments/${documentId}`, null, { base: 'tenant' })
    return apiResponse.data
  }

  async getDocumentsByOppAndId (oppId, documentId) {
    const apiResponse = await api.get(`opportunities/${oppId}/document/${documentId}`, null, { base: 'tenant' })
    return apiResponse.data
  }

  async getOpportunityById (id) {
    const BASE_MAP = {
      // 'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    const apiResponse = await api.get(`opportunities/${id}`, null, apiOpts)
    let data = apiResponse.data
    if (this.portal === PORTAL.INVESTOR) {
      const { opportunity, ...rest } = data
      data = {
        ...opportunity,
        ...rest,
      }
    }

    return data
  }

  async getDocumentsByOppId (id, params = {}) {
    const BASE_MAP = {
      // 'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    let url = ''
    if (this.portal === PORTAL.INVESTOR) {
      url = `opportunities/${id}/dataroom`
    } else if (this.portal === PORTAL.FID_ADMIN) {
      url = `opportunities/${id}/dataroom`
    } else {
      if (params.resource === 'issuer') {
        if (id) {
          url = `opportunities/${id}/document`
        } else {
          delete params.resource
          params.document_type = 'ISSUER_DOCUMENT'
          delete params['document_type.name']
          url = 'document'
        }
      } else {
        url = `opportunities/${id}/document`
      }
    }
    const apiResponse = await api.get(url, {
      params,
    }, apiOpts)
    return apiResponse.data
  }

  async getTenantDocumentsByOppId (id, params = {}) {
    const apiResponse = await api.get(`opportunities/${id}/compliance-document`, {
      params,
    })
    return apiResponse.data
  }

  async getIssuerDocuments (id, params = {}) {
    const BASE_MAP = {
      // 'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    let url = ''
    if (this.portal === PORTAL.INVESTOR) {
      url = 'document?document_type.name=ISSUER_DOCUMENT'
    } else if (this.portal === PORTAL.FID_ADMIN) {
      url = 'document?document_type.name=ISSUER_DOCUMENT'
    } else {
      url = 'document?document_type.name=ISSUER_DOCUMENT'
    }
    const apiResponse = await api.get(url, {
      params,
    }, apiOpts)
    return apiResponse.data
  }

  async getDLInvestmentByOppId (oppId) {
    const apiResponse = await api.get('investments', {
      params: {
        opportunity_id: oppId,
      },
    }, { base: 'tenant' })
    if (apiResponse.data && apiResponse.data.results && apiResponse.data.results.length > 0) {
      return apiResponse.data.results
    } else {
      return []
    }
  }

  async getDLInvestmentById (investorId, oppId, currentEntityId) {
    const apiResponse = await api.get('investments', {
      params: {
        investor_trellis_uid: investorId,
        opportunity_id: oppId,
      },
    }, { base: 'tenant' })
    if (apiResponse.data && apiResponse.data.results && apiResponse.data.results.length > 0) {
      return apiResponse.data.results.find(res => res.entity && res.entity.id === currentEntityId)
    } else {
      return []
    }
  }

  async getOneIvestment (id) {
    const response = await api.get(`investments/${id}`)
    return response.data
  }

  async getOppWhiteListedEnities (id) {
    const response = await api.get(`entities/investors/opportunities/${id}`, null, { base: 'tenant' })
    return response.data
  }

  async submitDLIoi (oppId, amount) {
    const apiResponse = await api.post(`opportunities/${oppId}/ioi`, {
      ioi_amount: amount,
    }, { base: 'tenant-entity-investor' })

    return apiResponse.data
  }

  async viewDLRequiredOppDoc (oppId, docId) {
    const apiResponse = await api.post(`opportunities/${oppId}/document/${docId}/viewed`, {
    }, { base: 'tenant-investor' })

    return apiResponse.data
  }

  async viewDLRequiredTenantDoc (oppId, docId) {
    const apiResponse = await api.post(`opportunities/${oppId}/compliance-document/${docId}/viewed`, {
    }, { base: 'tenant-investor' })

    return apiResponse.data
  }

  async getOpportunityFormsById (id) {
    const BASE_MAP = {
      'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    // same for all urls
    // const URL_MAP = {
    // }

    return api.get(`opportunities/${id}/forms`, null, apiOpts)
      .then(res => res.data)
      .catch(() => ([]))
  }

  async getOpportunityFormsByIdByFormName (id, formName) {
    const BASE_MAP = {
      'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    // same for all urls
    // const URL_MAP = {
    // }

    return api.get(`opportunities/${id}/forms/${formName}`, null, apiOpts)
  }

  async getIssuerFormsByIdByFormName (issuerId, formName) {
    const BASE_MAP = {
      'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    const URL_MAP = {
      'digital-locker': `issuer/${issuerId}/forms/${formName}`,
      'fid-admin': `issuer/${issuerId}/forms/${formName}`,
      issuers: `forms/${formName}`,
    }

    return api.get(URL_MAP[this.portal], null, apiOpts)
  }

  async loadOpportunityForms (id) {
    let forms = await this.getOpportunityFormsById(id)

    const formTempls = await Promise.all(
      forms.map(form =>
        formSrvc.getFormById(form.internal_name, { base: 'common' })
        .catch(() => { return null }),
        ),
      )
    let responses = await Promise.all(forms.map(form => {
      return this
        .getOpportunityFormsByIdByFormName(id, form.internal_name)
        .catch(() => {
          return null
        })
    }))
    responses = responses.map(res => {
      if (res && res.data && res.data.length > 0) {
        return res.data[0]
      } else {
        return { response: null }
      }
    })
    forms = forms.map((form, index) => {
      return {
        ...form,
        response: responses[index],
        formTempl: formTempls[index],
      }
    })

    this.oppFormData = forms
    return forms
  }

  async loadIssuerFormByPortal (issuerId) {
    const BASE_MAP = {
      'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    const URL_MAP = {
      'digital-locker': `issuer/${issuerId}/forms`,
      'fid-admin': `issuer/${issuerId}/forms`,
      issuers: 'forms',
    }

    const forms = await api.get(URL_MAP[this.portal], null, apiOpts)
      .then(res => res.data)
      .catch(() => ([]))

    return forms
  }

  async loadIssuerForms (issuerId) {
    let forms = await this.loadIssuerFormByPortal(issuerId)

    const formTempls = await Promise.all(forms.map(form => formSrvc.getFormById(form.internal_name, { base: 'common' })))
    let responses = await Promise.all(forms.map(form => {
      return this
        .getIssuerFormsByIdByFormName(issuerId, form.internal_name)
        .catch(() => {
          return null
        })
    }))
    responses = responses.map(res => {
      if (res && res.data && res.data.length > 0) {
        return res.data[0]
      } else {
        return { reponse: null }
      }
    })
    forms = forms.map((form, index) => {
      return {
        ...form,
        response: responses[index],
        formTempl: formTempls[index],
      }
    })

    this.issuerFormData = forms
    return forms
  }

  async loadDealDocument (id) {
    const BASE_MAP = {
      'digital-locker': 'tenant-entity-investor',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    return api.post(`opportunities/${id}/deal-document`, null, apiOpts)
  }

  getFormData (id) {
    return parseForm(id, this.oppFormData, this.issuerFormData)
  }

  getDrMode () {
    const mode = this.$route.query.mode
    if (mode) {
      return mode
    } else {
      return this.CONSTS.DR_MODES.PRESENTATION
    }
  }

  async markDocumentFav (id, doc) {
    const BASE_MAP = {
      // 'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    let url = ''
    if (this.portal === PORTAL.ISSUER) {
      url = `opportunities/${id}/document/${doc.document_id}/favourite`
    } else if (this.portal === PORTAL.FID_ADMIN) {
      if (doc && doc.document_type === 'COMPLIANCE') {
        url = `opportunities/${id}/compliance-document/${doc.document_id}/favourite`
      } else {
        url = `opportunities/${id}/dataroom/document/${doc.document_id}/favourite`
      }
    } else {
      if (doc && doc.document_type === 'COMPLIANCE') {
        url = `opportunities/${id}/compliance-document/${doc.document_id}/favourite`
      } else {
        url = `opportunities/${id}/dataroom/document/${doc.document_id}/favourite`
      }
    }
    const apiResponse = await api.post(url, {
    }, apiOpts)
    return apiResponse.data
  }

  async removeDocumentFav (id, doc) {
    const BASE_MAP = {
      // 'digital-locker': 'tenant',
    }
    const apiOpts = BASE_MAP[this.portal]
      ? { base: BASE_MAP[this.portal] }
      : null

    let url = ''
    if (this.portal === PORTAL.ISSUER) {
      url = `opportunities/${id}/document/${doc.document_id}/favourite`
    } else if (this.portal === PORTAL.FID_ADMIN) {
      if (doc && doc.document_type === 'COMPLIANCE') {
        url = `opportunities/${id}/compliance-document/${doc.document_id}/favourite`
      } else {
        url = `opportunities/${id}/dataroom/document/${doc.document_id}/favourite`
      }
    } else {
      if (doc && doc.document_type === 'COMPLIANCE') {
        url = `opportunities/${id}/compliance-document/${doc.document_id}/favourite`
      } else {
        url = `opportunities/${id}/dataroom/document/${doc.document_id}/favourite`
      }
    }
    const apiResponse = await api.delete(url, apiOpts)
    return apiResponse.data
  }

  async getFinancials (id, options) {
    let apiResponse = null
    try {
      if (this.portal === PORTAL.ISSUER) {
        apiResponse = await api.get(`opportunities/${id}/trade-data/financials`, null)
      } else {
        apiResponse = await api.get(`trade-data/opportunities/${id}/financials`, null, { base: 'tenant' })
      }
      apiResponse = apiResponse && apiResponse.data && apiResponse.data.results && apiResponse.data.results.length > 0
        ? apiResponse.data.results[0]
        : null
    } catch (err) {
      // pass
    }

    const financialData = this.getFormData('financials')
    if (financialData) {
      const incomeStatements = financialData['DRS.F.PL'] || []
      const cashFlows = financialData['DRS.F.CF'] || []
      const balanceSheets = financialData['DRS.F.BS'] || []
      const data = {
        ...apiResponse,
        opportunity_id: id,
        rev_data: incomeStatements.map(ele => ({ year: ele['DRS.F.PL.Year'], value: +ele['DRS.F.PL.Revenue'] })),
        exp_data: incomeStatements.map(ele => ({ year: ele['DRS.F.PL.Year'], value: +ele['DRS.F.PL.Expenses'] })),
        ebitda_data: incomeStatements.map(ele => ({ year: ele['DRS.F.PL.Year'], value: +ele['DRS.F.PL.EBITDA'] })),
        cfin_data: cashFlows.map(ele => ({ year: ele['DRS.F.CF.Year'], value: +ele['DRS.F.CF.In'] })),
        cfout_data: cashFlows.map(ele => ({ year: ele['DRS.F.CF.Year'], value: +ele['DRS.F.CF.Out'] })),

        bl_assets_data: balanceSheets.map(ele => ({ year: ele['DRS.F.BS.Year'], value: +ele['DRS.F.BS.Assets'] })).filter(ele => ele.year),
        bl_liab_data: balanceSheets.map(ele => ({ year: ele['DRS.F.BS.Year'], value: +ele['DRS.F.BS.Liabiliites'] })).filter(ele => ele.year),
        bl_equity_data: balanceSheets.map(ele => ({ year: ele['DRS.F.BS.Year'], value: +ele['DRS.F.BS.Equity'] })).filter(ele => ele.year),
      }
      return data
    }
    if (env === 'dev') {
      return financialSampleData
    }
    return apiResponse
  }

  async loadAllDataRoomDocuments (oppId, required, isTenantPortal) {
    const optsOppDocument = {
      'document_type.name': 'DATA_ROOM_DOCUMENT',
      // resource: 'issuer' | 'opportunity'
      page: 1,
      limit: 1000000,
    }
    if (required) {
      optsOppDocument['document_type.is_required'] = 'True'
    }

    const optsTenantDocument = {
      page: 1,
      limit: 1000000,
    }
    if (required) {
      optsTenantDocument.is_required = 'True'
    }

    // Load opportunity documents
    let oppDocumentsParsed = []
    try {
      const oppDocuments = await this.getDocumentsByOppId(oppId, { ...optsOppDocument, resource: 'opportunity' })

      oppDocumentsParsed = oppDocuments.results.map(doc => ({
        ...doc,
        loadFrom: DR_DOC_LOAD_FROM.OPP_DOC,
      }))
    } catch (err) {
      // ignore the error
    }

    // Load issuer documents
    let issuerDocmentsParsed = []
    try {
      const issuerDocuments = await this.getDocumentsByOppId(oppId, { ...optsOppDocument, resource: 'issuer' })

      issuerDocmentsParsed = issuerDocuments.results.map(doc => ({
        ...doc,
        loadFrom: DR_DOC_LOAD_FROM.ISSUER_DOC,
      }))
    } catch (err) {
      // ignore the error
    }
    let merged = oppDocumentsParsed.concat(issuerDocmentsParsed)

    try {
      if (isTenantPortal) {
        const tenantDocuments = await this.getTenantDocumentsByOppId(oppId, optsTenantDocument)
        const tenantDocumentsParsed = tenantDocuments.results.map(doc => ({
          ...doc,
          loadFrom: DR_DOC_LOAD_FROM.TENANT_DOC,
        }))
        merged = merged.concat(tenantDocumentsParsed)
      }
    } catch (err) {
      // ignore the error
    }
    const allDocuments = []

    // Remove duplication
    merged.forEach((item) => {
      const found = allDocuments.find(doc => doc.document_id === item.document_id)
      if (!found) {
        let isRequired = false
        if (item.loadFrom === DR_DOC_LOAD_FROM.TENANT_DOC) {
          isRequired = (item.is_required === true)
        } else {
          // For issuer and opp documents
          if (item.document_type && Array.isArray(item.document_type)) {
            item.document_type.forEach(type => {
              if (type.is_required) {
                isRequired = true
              }
            })
          }
        }

        allDocuments.push({
          ...item,
          isRequired,
        })
      }
    })

    return allDocuments
  }

  async viewDocument (loadFrom, oppId, docId) {
    if (loadFrom === DR_DOC_LOAD_FROM.TENANT_DOC) {
      await this.viewDLRequiredTenantDoc(oppId, docId)
    } else if (loadFrom === DR_DOC_LOAD_FROM.OPP_DOC) {
      await this.viewDLRequiredOppDoc(oppId, docId)
    } else if (loadFrom === DR_DOC_LOAD_FROM.ISSUER_DOC) {
      await this.viewDLRequiredOppDoc(oppId, docId)
    }
  }

  async getDealPreForm (id, intendedAudience, dealDocId) {
    const response = await api.get(`opportunities/${id}/deal/pre-form?intended_audience=${intendedAudience}&deal_doc_id=${dealDocId}`)
    return response.data
  }

  async createInvestment (oppId, entityId, formData, intendedAudience, dealDocId, custodianId) {
    const data = {
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      pre_form_response: formData,
      intended_audience: intendedAudience,
      deal_doc_id: dealDocId,
    }
    if (custodianId) {
      data.custodian_id = custodianId
    }

    const response = await api.post(`entity/${entityId}/opportunities/${oppId}/investment`, data)
    return response.data
  }

  async sendDealForApprove (investId) {
    const response = await api.post(`investments/${investId}/deal-document/review`, {}, { base: 'tenant' })
    return response.data
  }

  async saveDealFormData (id, data) {
    await api.patch(`investments/${id}/deal-document`, {
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      data_list: data,
    }, { base: 'tenant' })
  }

  async getDealESign (id, data = {}) {
    const response = await api.post(`investments/${id}/deal/esign`, data, { base: 'tenant' })
    return response.data
  }

  async getDealSignatoriesStatus (id) {
    const response = await api.get(`investments/${id}/deal/signatories`, null, { base: 'tenant' })
    return response.data
  }

  async getCrowdsmartScript (oppId) {
    const response = await api.get(`opportunities/${oppId}/crowdsmart/embedded-app`, null, { base: 'tenant' })
    return response.data
  }

  async getCrowdsmartReport (oppId, isPrivate = false) {
    const response = await api.get(`opportunities/${oppId}/crowdsmart/report${isPrivate ? '?private=True' : ''}`, null, { base: 'tenant' })
    return response.data
  }

  async createCrowdsmartPrivateEval (oppId) {
    const response = await api.post(`opportunities/${oppId}/crowdsmart/private-evaluation`, null, { base: 'tenant' })
    return response.data
  }

  async getOppInvestments (oppId, option) {
    const response = await api.get(`opportunities/${oppId}/investments/tracker`, null, option)
    return response.data
  }

  async sendInvitation (data) {
    const filteredData = {}
    Object.keys(data).forEach(key => {
      if (data[key]) {
        filteredData[key] = data[key]
      }
    })
    const response = await api.post('onboarding/invites', filteredData, { base: 'tenant' })
    return response.data
  }

  async getInvitation () {
    const response = await api.get('onboarding/investor/invite', null, { base: 'tenant' })
    return response.data
  }

  async getInvestorCustodian () {
    const response = await api.get('investor/custodian', null, { base: 'tenant' })
    return response.data
  }

  async getInvestorCustodianById (id, entityId) {
    const response = await api.get(`investor/entity/${entityId}/custodian/${id}`, null, { base: 'tenant' })
    return response.data
  }

  async signCustodianDocument (investId) {
    const response = await api.post(`investments/${investId}/custodian-document`, {}, { base: 'tenant' })
    return response.data
  }

  async updateCustodianDocument (investId, data) {
    await api.patch(`investments/${investId}/custodian-document`, {
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      custodian_data_list: data,
    }, { base: 'tenant' })
  }

  async getInvestorCustodianBalance (entityUuid, custodianName) {
    const response = await api.get(`investors/entity/${entityUuid}/custodian/${custodianName}/balance`, {
    }, { base: 'tenant' })
    return response.data
  }
}
