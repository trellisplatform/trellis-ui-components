import { api } from '@/api'
import { DR_DOC_LOAD_FROM } from '@/constants'

export const docIsViewed = (obj, key, docId) => {
  try {
    if (!obj || !obj[key] || Array.isArray[obj[key]]) {
      return false
    }

    return obj[key].map(doc => doc.doc_id).indexOf(docId) !== -1
  } catch {
    return false
  }
}

export const docNotAlreadyViewed = (loadFrom, alreadyViewedDocs, docId) => {
  return (
    loadFrom === DR_DOC_LOAD_FROM.OPP_DOC &&
    !docIsViewed(alreadyViewedDocs, 'data_room_docs', docId)
  ) || (
    loadFrom === DR_DOC_LOAD_FROM.TENANT_DOC &&
    !docIsViewed(alreadyViewedDocs, 'tenant_compliance_docs', docId)
  )
}

export const issuerDocumentBulkDelete = async (docIds) => {
  await api.delete('document/bulk-import', null, {
    document_ids: docIds,
  })
}

export const oppDocumentBulkDelete = async (oppId, docIds) => {
  await api.delete(`opportunities/${oppId}/document/bulk-import`, null, {
    document_ids: docIds,
  })
}

export const issuerBulkCategoryUpdate = async (payload) => {
  await api.patch('document/bulk-update/category', payload, { base: 'issuer' })
}
