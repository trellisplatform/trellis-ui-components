// import { api } from '@/api'
// import { PORTAL, DR_DOC_LOAD_FROM } from '@/constants'
// import * as formSrvc from '@/components/common/services/forms'

import parseForm from '../parse-form'
// import { financialSampleData } from './financials'
// import { env } from '@/components/common/constants'
import * as CONSTS from '@/constants'

export default class TearsheetDigitalRoadshowService {
  constructor (portal) {
    this.portal = portal
    this.oppFormData = null
    this.issuerFormData = null
    this.tearsheetData = null
    this.CONSTS = CONSTS
  }

  getFormData (id) {
    return parseForm(id, this.oppFormData, this.issuerFormData)
  }

  getDrMode () {
    const mode = this.$route.query.mode
    if (mode) {
      return mode
    } else {
      return this.CONSTS.DR_MODES.PRESENTATION
    }
  }

  loadForms (tearsheetData) {
    this.tearsheetData = tearsheetData
    const issuerFormDetails = tearsheetData.form_details.issuer_forms_details
    const oppFormDetails = tearsheetData.form_details.opportunity_forms_details
    const commonFormDetails = tearsheetData.form_details.common_forms_details

    const issuerFormData = []

    for (const formId in issuerFormDetails) {
      issuerFormData.push({
        formTempl: commonFormDetails[formId],
        response: issuerFormDetails[formId],
        internal_name: formId,
      })
    }
    this.issuerFormData = issuerFormData

    const oppFormData = []
    for (const formId in oppFormDetails) {
      oppFormData.push({
        formTempl: commonFormDetails[formId],
        response: oppFormDetails[formId],
        internal_name: formId,
      })
    }
    this.oppFormData = oppFormData
  }

  getDocumentsByOppAndId (oppId, docId) {
    // if (this.tearsheetData.pitch_deck_document && this.tearsheetData.pitch_deck_document.document_id === docId) {
      if (this.tearsheetData.pitch_deck_document) {
      return this.tearsheetData.pitch_deck_document
    }
  }
}
