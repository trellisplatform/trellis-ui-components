import { env } from './env'

export const PORTAL_BRANDING_BASE_URL = `${location.protocol}//s3.amazonaws.com/${env}.trellisplatform.com-branding`

export const PORTAL_BRANDING_JSON_BASE_URL = `${PORTAL_BRANDING_BASE_URL}/portal-branding`

export const PORTAL_BRANDING_JSON_URL = `${PORTAL_BRANDING_BASE_URL}/portal-branding/${location.hostname}.json`
