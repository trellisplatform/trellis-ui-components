import { api } from '@/api'

export const createRequest = async (oppId) => {
  const apiResponse = await api.post('service-request', {
    opportunity_id: oppId,
    type: 'DOWNLOAD_ALL_DATAROOM_DOCUMENTS',
  }, { base: 'tenant' })
  return apiResponse
}

export const getRequestList = async (page = 1, limit = 10, filter, base = 'tenant') => {
  const apiResponse = await api.get('service-request', {
    params: { page, limit, ...filter },
  }, { base: base })
  return apiResponse.data
}

export const getRequestDetail = async (id, base = 'tenant') => {
  const apiResponse = await api.get(`service-request/${id}`, null, { base: base })
  return apiResponse.data
}

export const retryPaxosAction = async (id) => {
  const apiResponse = await api.put(`service-request/${id}`, null, { base: 'tenant' })
  return apiResponse.data
}

export const downloadSelectedDocsRequest = async (docIds, oppId, downloadAll = false) => {
  let payload = {
    opportunity_id: oppId,
    type: 'DOWNLOAD_ALL_DATAROOM_DOCUMENTS',
  }
  if (!downloadAll) {
    payload = {
      ...payload,
      documents_id: docIds,
    }
  }
  const apiResponse = await api.post('service-request', payload, { base: 'tenant' })
  return apiResponse
}
