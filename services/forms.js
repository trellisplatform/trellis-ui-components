import { api } from '@/api'
import { composeGetQuery } from '@/services/utils'

const defaultForm = {
  name: '',
  description: '',
  opening: '',
  closing: '',
  sections: [],
  is_published: false,
}

export const createNewForm = (source = '') => {
  return { ...defaultForm, source }
}

export const getForms = async (options, apiOpts) => {
  const params = composeGetQuery(options)
  const apiResponse = await api.get('forms', { params }, apiOpts)

  return apiResponse.data
}

export const getFormById = async (id, apiOpts) => {
  const apiResponse = await api.get(`forms/${id}`, null, apiOpts)
  return apiResponse.data
}
