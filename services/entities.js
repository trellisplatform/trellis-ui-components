import { api } from '@/api'
import { composeGetQuery } from '@/services/utils'

export const getList = async (options, apiOpts) => {
  const params = composeGetQuery(options)
  const apiResponse = await api.get('entities', { params }, apiOpts)

  return apiResponse.data
}

export const getById = async (id, options, apiOpts) => {
  const apiResponse = await api.get(`entities/${id}`, { }, apiOpts)

  return apiResponse.data
}

export const getOpportunitiesByEntity = async (id, options, apiOpts) => {
  const params = composeGetQuery(options)
  const apiResponse = await api.get(`entities/${id}/opportunities`, { params }, apiOpts)

  return apiResponse.data
}

export const addTagsToEntity = async (id, tags) => {
  const apiResponse = await api.post(`entities/${id}/entity-tag`, { tags })

  return apiResponse.data
}
export const removeTagsFromEntity = async (id, tags) => {
  const apiResponse = await api.delete(`entities/${id}/entity-tag`, null, { tags })

  return apiResponse.data
}

export const getStats = async (entityUid, apiOpts) => {
  const apiResponse = await api.get(`entities/${entityUid}/stats`, { }, apiOpts)

  return apiResponse.data
}
