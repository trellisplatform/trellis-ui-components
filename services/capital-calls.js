import { api } from '@/api'
import { composeGetQuery } from '@/services/utils'

export const previewCapitalCalls = async (id, params, apiOpts) => {
  const response = await api.post(`opportunities/${id}/capital-call/preview`, params, apiOpts)
  return response.data
}

export const getCapitalCalls = async (id, options, apiOpts) => {
  const params = composeGetQuery(options)
  const response = await api.get(`opportunities/${id}/capital-call`, { params }, apiOpts)
  return response.data
}

export const getInvestorsStats = async (id) => {
  const response = await api.get(`opportunities/${id}/capital-call/investors/stats`)
  return response.data
}
export const draftCapitalCall = async (id, params, apiOpts) => {
  const response = await api.post(`opportunities/${id}/capital-call`, params, apiOpts)
  return response.data
}

export const publishCapitalCalls = async (id, scheduleId, params, apiOpts) => {
  const response = await api.post(`opportunities/${id}/capital-call/${scheduleId}`, params, apiOpts)
  return response.data
}

export const sendEmailReminder = async (id, params, apiOpts) => {
  const response = await api.post(`opportunities/${id}/capital-call/email`, params, apiOpts)
  return response.data
}

export const deleteCapitalCall = async (id, scheduleId, apiOpts) => {
  const response = await api.delete(`opportunities/${id}/capital-call/${scheduleId}`, apiOpts)
  return response.data
}

export const editCapitalCall = async (id, scheduleId, params, apiOpts) => {
  const response = await api.put(`opportunities/${id}/capital-call/${scheduleId}`, params, apiOpts)
  return response.data
}

export const updateCapitalCall = async (id, capitalCallId, params, apiOpts) => {
  const response = await api.patch(`opportunities/${id}/capital-call/${capitalCallId}`, params, apiOpts)
  return response.data
}

export const getPendingInvestments = async (id, scheduleId, isPending, apiOpts) => {
  const response = await api.get(`opportunities/${id}/capital-call/${scheduleId}/investments?is_pending=${isPending}`, isPending, apiOpts)
  return response.data
}

export const addInvestmentsToCapitalCall = async (id, scheduleId, params, apiOpts) => {
  const response = await api.post(`opportunities/${id}/capital-call/${scheduleId}/investments`, params, apiOpts)
  return response.data
}

export const previewLastCapitalCall = async (id, dueDate, apiOpts) => {
  const response = await api.get(`opportunities/${id}/capital-call/last-schedule?due_date=${dueDate}`, apiOpts)
  return response.data
}
