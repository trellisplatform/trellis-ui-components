import { api } from '@/api'
import { composeGetQuery } from '@/services/utils'
import { $getPortal } from '@/components/common/mixins'
import { PORTAL } from '@/constants'

export const getSystemNotifications = async (options) => {
  const portal = $getPortal()
  const url = portal === PORTAL.ISSUER ? 'system-notifications' : 'accounts/system-notifications'

  const params = composeGetQuery(options)

  const baseMap = {
    [PORTAL.INVESTOR]: 'tenant',
  }
  const base = baseMap[portal] || ''
  const apiResponse = await api.get(url, { params }, { base })

  return apiResponse.data
}
