import { API_BASE_URL } from '@/constants'
import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: API_BASE_URL,
  params: {},
  timeout: 0,
})

export const verifyToken = async (token) => {
  const response = await axiosInstance.get('/admin/verify-token', {
    headers: {
      AUTHORIZATION: token,
    },
  })
  return response.data
}

export const setPassword = async ({ username, password, token }) => {
  const response = await axiosInstance.post('/admin/set-password', {
    username,
    password,
  }, {
    headers: {
      AUTHORIZATION: token,
    },
  })
  return response.data
}

export const resetPasswordByToken = async ({ username, password, token }) => {
  const response = await axiosInstance.post('/admin/reset-password-by-token', {
    username,
    password,
  }, {
    headers: {
      AUTHORIZATION: token,
    },
  })
  return response.data
}

export const isValidSetPasswordToken = async (token) => {
  const response = await axiosInstance.post('/admin/is-valid-set-password', null, {
    headers: {
      AUTHORIZATION: token,
    },
  })
  return response.data
}
