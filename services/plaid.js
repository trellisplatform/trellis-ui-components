import { api } from '@/api'
import { composeGetQuery } from '../../../services/utils'

export const getLinkToken = async (entityUid) => {
  const apiResponse = await api.post(
    `wire-transfer/pdi/entity/${entityUid}/link-token`, {
      country_codes: ['US'],
      products: ['auth'],
    },
    { base: 'tenant' },
  )
  return apiResponse.data
}

export const addAccount = async (entityUid, data) => {
  const apiResponse = await api.post(`wire-transfer/pdi/entity/${entityUid}/access-token`, data, {
    base: 'tenant',
  })
  return apiResponse.data
}

export const getAccounts = async (entityUid, options) => {
  const params = composeGetQuery(options)

  const apiResponse = await api.get(`wire-transfer/pdi/entity/${entityUid}/balance`, { params }, {
    base: 'tenant',
  })
  return apiResponse.data
}

export const submitDwolla = async (entityUid, body) => {
  const apiResponse = await api.post(`wire-transfer/pdi/entity/${entityUid}/dwolla/config`, body, {
    base: 'tenant',
  })
  return apiResponse.data
}
