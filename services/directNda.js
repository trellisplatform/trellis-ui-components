import { api } from '@/api'
// import { composeGetQuery } from '@/services/utils'

export const getIssuerNotes = async (id, apiOpts) => {
  const apiResponse = await api.get(`opportunities/investors/${id}/nda/note`, null, apiOpts)
  return apiResponse.data
}

export const getInvestorNotes = async (id, apiOpts) => {
  const apiResponse = await api.get(`investors/opportunities/${id}/nda/note`, null, apiOpts)
  return apiResponse.data
}

export const sendRejectNoteAndDeclineNda = async (id, params, apiOpts) => {
  const apiResponse = await api.patch(`opportunities/investors/${id}/nda/note`, params, apiOpts)
  await api.post(`opportunities/investments/${id}/nda/status`, { nda_status: 'NDA_DECLINED' })
  return apiResponse.data
}

export const uploadNdaWithReason = async (id, params, apiOpts) => {
  const apiResponse = await api.post(`investors/opportunities/${id}/nda/note`, params, apiOpts)
  return apiResponse.data
}

export const sendNoteToInvestor = async (id, params, apiOpts) => {
  const apiResponse = await api.patch(`opportunities/investors/${id}/nda/note`, params, apiOpts)
  return apiResponse.data
}

export const sendNoteToIssuer = async (id, params, apiOpts) => {
  const apiResponse = await api.post(`investors/opportunities/${id}/nda/note`, params, apiOpts)
  return apiResponse.data
}

export const ndaDirectUpload = async (id, file, note, progressCallback) => {
  const formData = new FormData()
  const info = {
    note: note,
  }
  formData.append('file', file)
  formData.append('request_payload', JSON.stringify(info))
  const response = await api.upload(`opportunities/${id}/nda/direct-sign`, formData, {}, progressCallback)
  return response.data
}
