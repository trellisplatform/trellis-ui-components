import { api } from '@/api'

export const getInvestorCustodianEsignUrl = async (entityUuid, custodianId) => {
  const response = await api.get(`investors/entity/${entityUuid}/custodian/${custodianId}/esign`, {
  }, { base: 'tenant' })
  return response.data
}

export const getCustodianById = async (custodianId) => {
  const response = await api.get(`custodian/${custodianId}`, {
  }, { base: 'tenant' })
  return response.data
}

export const saveCustodianDocument = async (id, data) => {
  await api.patch(`investments/${id}/custodian-document`, {
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
    data_list: data,
  }, { base: 'tenant' })
}

export const submitCustodianDocument = async (id) => {
  await api.post(`investments/${id}/custodian-document`, {
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
  }, { base: 'tenant' })
}
