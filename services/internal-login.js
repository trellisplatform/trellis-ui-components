import { api } from '@/api'

const STORAGE_KEY = 'trellis-internal-login'

 export const setInternalLoginData = async (data) => {
  localStorage.setItem(STORAGE_KEY, JSON.stringify(data))
}

/**
  * @params orgType - Fiduciary|Issuer
  */
export const loginPortal = async (orgId, orgType) => {
  const body = {
    organization_id: orgId,
    organization_type: orgType,
  }
  const apiResponse = await api.post('internal-login', body)

  localStorage.setItem(STORAGE_KEY, JSON.stringify(apiResponse.data))

  let portal = ''
  if (orgType === 'Fiduciary') {
    portal = 'fid-admin'
  } else if (orgType === 'Issuer') {
    portal = 'issuers'
  }

  const url = `${location.origin}/${portal}/pages/internal-login`
  window.open(url, '_blank')
  return apiResponse.data
}

export const loginFidFromIssuer = async () => {
  const apiResponse = await api.post('fiduciary/login')

  localStorage.setItem(STORAGE_KEY, JSON.stringify(apiResponse.data))

  const url = `${location.origin}/fid-admin/pages/internal-login-from-issuer`
  window.open(url, '_blank')
  return apiResponse.data
}

export const loginIssuerFromFid = async () => {
  const apiResponse = await api.post('accounts/issuer/login', { base: 'tenant' })

  localStorage.setItem(STORAGE_KEY, JSON.stringify(apiResponse.data))

  const url = `${location.origin}/issuers/pages/internal-login-from-fid`
  window.open(url, '_blank')
  return apiResponse.data
}

export const loginDigitalLockerFromFid = async (entityId) => {
  const apiResponse = await api.post(`investors/${entityId}/login`)

  localStorage.setItem(STORAGE_KEY, JSON.stringify(apiResponse.data))

  const url = `${location.origin}/digital-locker/pages/internal-login`
  window.open(url, '_blank')
  return apiResponse.data
}

export const getSavedResponse = async (orgId, orgType) => {
  try {
    return JSON.parse(localStorage.getItem(STORAGE_KEY)) || null
  } catch (err) {
    return null
  }
}
