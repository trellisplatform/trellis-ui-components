import { api } from '@/api'

export const uploadOpportunityDocument = async (oppId, doc, isNew, docId, progressCallback) => {
  const formData = new FormData()
  formData.append('file', doc.file)

  const info = {
    category: doc.category,
    name: doc.name,
    document_type: doc.document_type,
    description: doc.description,
    details: doc.details,
    data_list: doc.data_list,
  }
  formData.append('request_payload', JSON.stringify(info))

  let url = ''
  url = `opportunities/${oppId}/document`
  if (!isNew) {
    url += `/${docId}` || ''
  }

  let createdDoc = null
  if (!doc.file) {
    // run patch
    createdDoc = await api.patch(
      url,
      info,
    )
  } else {
    createdDoc = await api.upload(
      url,
      formData,
      {},
      progressCallback,
      isNew ? 'post' : 'put',
    )
  }

  return createdDoc.data
}

export const uploadTenantDocument = async (oppId, doc, isNew, docId, progressCallback) => {
  const formData = new FormData()
  formData.append('file', doc.file)

  const info = {
    category: doc.category,
    name: doc.name,
    document_type: doc.document_type,
    description: doc.description,
    details: doc.details,
    data_list: doc.data_list,
    is_private: doc.is_private,
    is_required: doc.is_required,
    // is_important: doc.is_important,    is_important field is specifically handled at the end
  }
  formData.append('request_payload', JSON.stringify(info))

  let url = ''
  url = `opportunities/${oppId}/compliance-document`
  if (!isNew) {
    url += `/${docId}` || ''
  }

  let createdDoc = null
  if (!doc.file) {
    // run patch
    createdDoc = await api.patch(
      url,
      info,
    )
  } else {
    createdDoc = await api.upload(
      url,
      formData,
      {},
      progressCallback,
      isNew ? 'post' : 'put',
    )
  }

  return createdDoc.data
}

export const markImportant = async (oppId, docId, important) => {
  const url = `opportunities/${oppId}/compliance-document/${docId}/important`
  if (important) {
    await api.post(url)
  } else {
    await api.delete(url)
  }
}

export const updateBlacklistTag = async (oppId, docId, tags) => {
  // TODO: Confirm api method with @Haris TPI-14215
  const url = `opportunities/${oppId}/compliance-document/${docId}/access`
  await api.post(url, {
    entity_tag_ids: tags,
  })
}

export const getOppDefaultDoc = () => ({
  // default
  name: '',
  description: '',
  datalist: [],
  details: {
    download_allowed: false,
    watermark: false,
  },
  document_type: [],
  is_issuer_private: false,
})

export const getTenantDefaultDoc = () => ({
  // default
  name: '',
  description: '',
  datalist: [],
  details: {
    download_allowed: false,
    watermark: false,
  },
  document_type: [],
  is_required: false,
  is_private: true,
})

export const uploadIssuerDocumentV2 = async (oppId, doc, isNew, docId, progressCallback) => {
  const formData = new FormData()
  formData.append('file', doc.file)

  const info = {
    category: doc.category,
    name: doc.name,
    document_type: doc.document_type,
    description: doc.description,
    details: doc.details,
    data_list: doc.data_list,
  }
  formData.append('request_payload', JSON.stringify(info))

  let url = ''
  url = `opportunities/${oppId}/document`
  if (!isNew) {
    url += `/${docId}` || ''
  }

  let createdDoc = null
  if (!doc.file) {
    // run patch
    createdDoc = await api.patch(
      url,
      info,
    )
  } else {
    createdDoc = await api.upload(
      url,
      formData,
      {},
      progressCallback,
      isNew ? 'post' : 'put',
    )
  }

  return createdDoc.data
}

export const uploadIssuerDocument = async (oppId, doc, isNew, docId, progressCallback) => {
  const formData = new FormData()
  formData.append('file', doc.file)

  const info = {
    category: doc.category,
    name: doc.name,
    document_type: doc.document_type,
    description: doc.description,
    details: doc.details,
    data_list: doc.data_list,
    data_room_document: true,
    is_issuer_private: doc.is_issuer_private,
    is_required: doc.is_required,
  }
  formData.append('request_payload', JSON.stringify(info))

  let url = ''
  url = 'document'
  if (!isNew) {
    url += `/${docId}` || ''
  }

  let createdDoc = null
  if (!doc.file) {
    // run patch
    createdDoc = await api.patch(
      url,
      info,
    )
  } else {
    createdDoc = await api.upload(
      url,
      formData,
      {},
      progressCallback,
      isNew ? 'post' : 'put',
    )
  }

  return createdDoc.data
}

export const deleteDocument = async (id) => {
  const res = await api.delete(`document/${id}`)

  return res
}

/*
 * mark opportunity document as issuer private
 */
export const setOppDocPrivate = async (oppId, id) => {
  const res = await api.post(`opportunities/${oppId}/document/${id}/private`)

  return res
}

/*
 * remove opportunity document as issuer private
 */
export const removeOppDocPrivate = async (oppId, id) => {
  const res = await api.delete(`opportunities/${oppId}/document/${id}/private`)

  return res
}
/*
 * mark tenant document as issuer private
 */
export const setTenantDocPrivate = async (oppId, id) => {
  const res = await api.post(`opportunities/${oppId}/compliance-document/${id}/private`)

  return res
}

/*
 * remove tenant document as issuer private
 */
export const removeTenantDocPrivate = async (oppId, id) => {
  const res = await api.delete(`opportunities/${oppId}/compliance-document/${id}/private`)

  return res
}

/*
 * mark dataroom document as issuer private
 */
export const setDataroomDocPrivate = async (oppId, id) => {
  const res = await api.post(`opportunities/${oppId}/dataroom/document/${id}/private`)

  return res
}

/*
 * remove dataroom document as issuer private
 */
export const removeDataroomDocPrivate = async (oppId, id) => {
  const res = await api.delete(`opportunities/${oppId}/dataroom/document/${id}/private`)

  return res
}

/*
 * upload bulk file
 */
export const uploadBulkImport = async (oppId, doc, progressCallback) => {
  const formData = new FormData()
  formData.append('file', doc.file)

  const info = {
    details: doc.details,
    retain_duplicates: doc.retain_duplicates,
  }
  formData.append('attributes', JSON.stringify(info))

  const url = oppId
    ? `opportunities/${oppId}/document/bulk-import`
    : 'document/bulk-import'

  const createdDocResp = await api.upload(
    url,
    formData,
    {},
    progressCallback,
    'post',
  )

  return createdDocResp.data
}

export const addDocReviewLog = async (oppId, id, details) => {
  const res = await api.post(`opportunities/${oppId}/document/${id}/review-log`, details, { base: 'tenant' })

  return res
}
