import { api } from '@/api'
import { composeGetQuery } from '@/services/utils'

const defaultSection = {
  name: '',
  description: '',
  multiple: false,
  definition: [],
}

export const createNewSection = (source = '') => {
  return { ...defaultSection, source }
}

export const getSections = async (options, apiOpts) => {
  const params = composeGetQuery(options)
  const apiResponse = await api.get('sections', { params }, apiOpts)

  return apiResponse.data
}

export const getSectionById = async (id, apiOpts) => {
  const apiResponse = await api.get(`sections/${id}`, null, apiOpts)
  return apiResponse.data
}
