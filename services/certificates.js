import { api } from '@/api'
import store from '@/store'

export const getCertificates = async (recipient, field = 'recipient_username') => {
  const user = store.getters['auth/user']

  const params = {
    [field]: recipient || user.username,
  }

  const apiResponse = await api.get('certificates', { params }, { base: 'common' })
  return apiResponse.data
}
