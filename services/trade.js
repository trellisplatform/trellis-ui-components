import { api } from '@/api'

export const getTrades = async () => {
  const response = await api.get('trade-data', null, { base: 'tenant' })

  return response.data
}

export const getTradeToken = async (email, entityId) => {
  const response = await api.get(`entities/${entityId}/trade_paxos/get-token`, null, { base: 'tenant' })

  return response.data
}

export const getUserBalance = async (entityId) => {
  const response = await api.get(`trade-data/${entityId}/balance`, null, { base: 'tenant' })
  return response.data
}

export const getTradeEntities = async () => {
  const response = await api.get('trade-data/details', null, { base: 'tenant' })
  return response.data.results
}
