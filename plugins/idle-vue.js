import Vue from 'vue'
import IdleVue from 'idle-vue'
import store from '@/store'

const eventsHub = new Vue()

Vue.use(IdleVue, {
  store,
  eventEmitter: eventsHub,
  idleTime: 1800000, // 30 minutes,
  startAtIdle: false,
})
