// const theme = {
//   primary: '#023c56',
//   secondary: '#b0c4da',
//   accent: '#f4f4f4',
//   info: '#000000',
//   error: '#F10808',
//   success: '#1FC824',
//   warning: '#e6a818',
// }

export default {
  primary: '#009F46',
  secondary: '#0075A1',
  accent: '#f4f4f4',
  info: '#000000',
  error: '#E55957',
  success: '#1DC366',
  warning: '#FE970C',

  trsText: {
    // Regular
    base: '#5D686B',
    // Dark
    darken1: '#394144',
    // Light
    lighten1: '#8A989B',
  },
  trsBack: {
    // mediumlight
    base: '#202C49',
    // dark
    darken1: '#323A3C',
    // lighten1
    lighten1: '#EDF2F3',
    // lighten2
    lighten2: '#D6DDDF',
  },
}
